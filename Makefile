OPHIS=../Ophis-2.1//ophis

all: step1_read_print.prg

step0_repl.prg: step0_repl.oph print.oph readline.oph
	$(OPHIS) -v $^

step1_read_print.prg: step1_read_print.oph print.oph readline.oph reader.oph stack.oph value.oph
	$(OPHIS) -v $^ -m test.map
